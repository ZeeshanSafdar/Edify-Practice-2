﻿namespace EdifyPractice.Common.Enums
{
    public enum UserType
    {
        SuperAdmin = 1,
        SchoolAdmin,
        Employee,
        Teacher,
        Student,
        Parent,
    }
}