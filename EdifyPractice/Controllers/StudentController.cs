﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace EdifyPractice.Controllers
{
    public class StudentController : Controller
    {
        public IActionResult StudentAdmission()
        {
            return View("~/Views/Student/StudentAdmission.cshtml");
        }

        public IActionResult StudentList()
        {
            return View("~/Views/Student/StudentList.cshtml");
        }
    }
}