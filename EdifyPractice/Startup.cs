﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using EdifyPractice.Data;
using EdifyPractice.Models;
using EdifyPractice.Services;

namespace EdifyPractice
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        #region Defining Roles
        // Defining Roles
        private async Task CreateUserRoles(IServiceProvider serviceProvider)
        {
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();

            IdentityResult RoleResult;
            //Adding Admin Role
            var adminRoleCheck = await roleManager.RoleExistsAsync("Admin");
            if (!adminRoleCheck)
            {
                //create the roles and seed them to the database
                RoleResult = await roleManager.CreateAsync(new IdentityRole("Admin"));
            }
            //Adding SuperAdmin Role
            var superAdminRoleCheck = await roleManager.RoleExistsAsync("SuperAdmin");
            if (!superAdminRoleCheck)
            {
                //create the roles and seed them to the database
                RoleResult = await roleManager.CreateAsync(new IdentityRole("SuperAdmin"));
            }
            //Adding Student Role
            var studentRoleCheck = await roleManager.RoleExistsAsync("Student");
            if (!studentRoleCheck)
            {
                //create the roles and seed them to the database
                RoleResult = await roleManager.CreateAsync(new IdentityRole("Student"));
            }
            //Adding Parent Role
            var parentRoleCheck = await roleManager.RoleExistsAsync("Parent");
            if (!parentRoleCheck)
            {
                //create the roles and seed them to the database
                RoleResult = await roleManager.CreateAsync(new IdentityRole("Parent"));
            }
            //Adding Teacher Role
            var teacherRoleCheck = await roleManager.RoleExistsAsync("Teacher");
            if (!teacherRoleCheck)
            {
                //create the roles and seed them to the database
                RoleResult = await roleManager.CreateAsync(new IdentityRole("Teacher"));
            }
            //Assign Admin role to the main User here we have given our newly registered 
            //login id for Admin management
            ApplicationUser userAdmin = await userManager.FindByEmailAsync("admin@gmail.com");
            var user = new ApplicationUser();
            await userManager.AddToRoleAsync(userAdmin, "Admin");
            userAdmin = await userManager.FindByEmailAsync("test@gmail.com");
            await userManager.AddToRoleAsync(userAdmin, "Admin");

            ApplicationUser userSuperAdmin = await userManager.FindByEmailAsync("superadmin@gmail.com");
            await userManager.AddToRoleAsync(userSuperAdmin, "SuperAdmin");

            ApplicationUser userTeacher = await userManager.FindByEmailAsync("teacher@gmail.com");
            await userManager.AddToRoleAsync(userTeacher, "Teacher");

            ApplicationUser userStudent = await userManager.FindByEmailAsync("student@gmail.com");
            await userManager.AddToRoleAsync(userStudent, "Student");

            ApplicationUser userParent = await userManager.FindByEmailAsync("parent@gmail.com");
            await userManager.AddToRoleAsync(userParent, "Parent");
            
        }

        #endregion




        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connection = @"Server=LRO-SH-41\SQLEXPRESS;Database=EdifyPractice;Trusted_Connection=True;ConnectRetryCount=0";
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(connection));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();


            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=FrontEnd}/{id?}");
            });
        }
    }
}
