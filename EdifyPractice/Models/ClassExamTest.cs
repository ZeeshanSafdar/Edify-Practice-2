﻿namespace EdifyPractice.Models
{
    public class ClassExamTest
    {
        public int Id { get; set; }

        public int ClassId { get; set; }
        public Class Class { get; set; }

        public int ExamTestId { get; set; }
        public ExamTest ExamTest { get; set; }
    }
}