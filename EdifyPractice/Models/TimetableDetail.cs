﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class TimetableDetail : BaseModel
    {

        [Required]
        [DataType(DataType.DateTime)]
        public string StartTime { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public string EndTime { get; set; }



        #region BELONGS TO FIELDS

        public string SubjectId { get; set; }

        public string TimetableId { get; set; }

        public string TeacherId { get; set; }


        public virtual Subject Subject { get; set; }
        public virtual Timetable Timetable { get; set; }
        public virtual Teacher Teacher { get; set; }

        #endregion


    }
}
