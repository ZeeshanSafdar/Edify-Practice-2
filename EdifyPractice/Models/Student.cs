﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace EdifyPractice.Models
{
    public class Student : BaseModel
    {
        [Required]
        public int RollNo { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime Admissiondate { get; set; }

        [Required]
        public double AdmissionFee{ get; set; }


        #region BELONGS TO FIELDS

        public int UserId { get; set; }
        public int ParentId { get; set; }
        public int ClassId { get; set; }
//        public int SectionId { get; set; }

        public virtual Parent Parent { get; set; }
        public virtual Class Class { get; set; }
        //        public virtual Section Section { get; set; }

        #endregion



        #region HAS MANY FIELDS

        //        public virtual Collection<Subject> Subjects { get; set; }
        //        public virtual Collection<Section> Sections { get; set; }
        public virtual Collection<StudentSubject> StudentSubjects { get; set; }
        public virtual Collection<SectionStudent> SectionStudents { get; set; }
        public virtual Collection<StuAttendanceDetail> StuAttendancesDetails { get; set; }
        public virtual Collection<SubjectResultDetail> SubjectResultDetails { get; set; }
        public virtual Collection<StudentGatePass> StudentGatePasses { get; set; }
        public virtual Collection<SpecialNote> SpecialNotes { get; set; }
        public virtual Collection<DailyDiary> DailyDiaries { get; set; }

        #endregion

    }
}
