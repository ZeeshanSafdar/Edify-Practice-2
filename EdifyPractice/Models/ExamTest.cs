﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class ExamTest : BaseModel
    {
        [Required]
        public string ExamName { get; set; }

        [Required]
        public string ExamType { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime StartDate { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime EndDate { get; set; }


        #region HAS MANY FIELDS

        //        public virtual Collection<Class> Classes { get; set; }
        public virtual Collection<SubjectResult> SubjectResults { get; set; }
        public virtual Collection<Syllabus> Syllabi { get; set; }
        public virtual Collection<Datesheet> Datesheets { get; set; }
        public virtual Collection<ClassExamTest> ClassExamTests { get; set; }

        #endregion



    }
}
