﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using EdifyPractice.Common.Enums;


namespace EdifyPractice.Models
{
    public class Branch : DefaultModel
    {
        
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [Phone]
        public string PhoneNumber { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public int TotalStudents{ get; set; }

        [Required]
        public Status Status { get; set; }

        
        #region BELONGS TO FIELDS

        public int SchoolId { get; set; }
        public virtual School School { get; set; }

        #endregion
        

        #region HAS MANY FIELDS

        public virtual Collection<User> Users { get; set; }
        public virtual Collection<Event> Events { get; set; }
        public virtual Collection<NewsUpdate> NewsUpdates { get; set; }
        public virtual Collection<Class> Classes { get; set; }

        #endregion





    }
}
