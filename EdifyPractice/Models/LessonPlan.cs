﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class LessonPlan : BaseModel
    {

        [Required]
        [DataType(DataType.Date)]
        public DateTime From { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime To { get; set; }


        #region BELONGS TO FIELDS

        public int SectionId { get; set; }

        public int TeacherId { get; set; }

        public int SubjectId { get; set; }

        public virtual Section Section { get; set; }
        public virtual Teacher Teacher { get; set; }
        public virtual Subject Subject { get; set; }

        #endregion


        #region HAS MANY FIELDS

        public virtual Collection<LessonPlanDetail> LessonPlanDetails { get; set; }

        #endregion

    }
}
