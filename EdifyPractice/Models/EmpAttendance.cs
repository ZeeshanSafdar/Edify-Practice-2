﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Conventions.Internal;

namespace EdifyPractice.Models
{
    public class EmpAttendance : BaseModel
    {
       
        [Required]
        public string Status { get; set; }

        [Required]
        public DateTime AttendanceDate { get; set; }



        #region BELONGS TO FIELDS

        public int EmpId { get; set; }

        public virtual Employee Employee { get; set; }


        #endregion


    }
}
