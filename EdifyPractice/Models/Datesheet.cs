﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class Datesheet : BaseModel
    {
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime Date { get; set; }

        [Required]
        public string Note { get; set; }


        #region BELONGS TO FIELDS

        public int ClassId { get; set; }
        public int ExamTestId { get; set; }

        public virtual Class Class { get; set; }
        public virtual ExamTest ExamTest { get; set; }

        #endregion



        #region HAS MANY FIELDS

        public virtual Collection<DatesheetDetail> DatesheetDetails { get; set; }

        #endregion


    }
}
