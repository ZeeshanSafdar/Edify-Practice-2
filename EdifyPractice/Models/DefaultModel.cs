﻿using System;

namespace EdifyPractice.Models
{
    public class DefaultModel : BaseModel
    {
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime DeletedAt { get; set; }
    }
}