﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class PayrollCategory : BaseModel
    {

        [Required]
        public double Allowance { get; set; }

        [Required]
        public double Bonus { get; set; }


        #region BELONGS TO FIELDS

        public int PayId { get; set; }

        public virtual Pay Pay { get; set; }

        #endregion

    }
}
