﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class SubjectResultDetail : BaseModel
    {
       

        [Required]
        [StringLength(100)]
        public string Remarks { get; set; }

        [Required]
        public int TotalMarks { get; set; }

        [Required]
        public int ObtainedMarks { get; set; }


        #region BELONGS TO FIELDS

        public int StudentId { get; set; }

        public int ExamTestId { get; set; }

        public int SubjectResultId { get; set; }

        public virtual Student Student { get; set; }

        public virtual ExamTest ExamTest { get; set; }

        public virtual SubjectResult SubjectResult { get; set; }

        #endregion



    }
}
