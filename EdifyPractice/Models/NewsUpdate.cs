﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace EdifyPractice.Models
{
    public class NewsUpdate : BaseModel
    {
       
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime Date { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Detail { get; set; }



        #region BELONGS TO FIELDS

        public int SchoolId { get; set; }

        public int BranchId { get; set; }

        public virtual School School { get; set; }
        public virtual Branch Branch { get; set; }

        #endregion



    }
}
