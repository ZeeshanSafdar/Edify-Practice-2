﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class Subject : BaseModel
    {


        #region BELONGS TO FIELDS

        //        public int ClassId { get; set; }

        #endregion


        #region HAS MANY FIELDS

        //        public virtual Collection<Class> Classes { get; set; }
        public virtual Collection<ClassSubject> ClassSubjects { get; set; }
        public virtual Collection<StudentSubject> StudentSubjects { get; set; }
        //        public virtual Collection<Student> Students { get; set; }
        //        public virtual Collection<Teacher> Teachers { get; set; }
        public virtual Collection<SubjectTeacher> SubjectTeachers { get; set; }
        public virtual Collection<LessonPlan> LessonPlans { get; set; }
        public virtual Collection<DatesheetDetail> DatesheetDetails { get; set; }
        public virtual Collection<TimetableDetail> TimetableDetails { get; set; }
        public virtual Collection<SyllabusDetail> SyllabusDetails { get; set; }
        public virtual Collection<SubjectResult> SubjectResults { get; set; }
        public virtual Collection<DailyDiaryDetail> DailyDiaryDetails { get; set; }

        #endregion


    }
}
