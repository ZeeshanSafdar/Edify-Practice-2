﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class StudentGatePass : BaseModel
    {
        [Required]
        public string Description { get; set; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateTime { get; set; }

        #region BELONGS TO FIELDS

        public int StudentId { get; set; }

        public virtual Student Student { get; set; }

        #endregion



    }
}
