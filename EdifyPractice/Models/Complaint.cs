﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class Complaint : BaseModel
    {
        [Required]
        public string ComplaintSubject { get; set; }

        public string ComplaintType { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public DateTime Date { get; set; }


        #region BELONGS TO FIELDS

        public int EmployeeId { get; set; }
   
        public int ParentId { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual Parent Parent { get; set; }

        #endregion


    }
}
