﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class SalarySlip : BaseModel
    {   

        [Required]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Required]
        public double Amount { get; set; }


        #region BELONGS TO FIELDS

        [Required]
        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        #endregion


        #region HAS MANY FIELDS

        public virtual Collection<Pay> Pays { get; set; }

        #endregion


    }
}
