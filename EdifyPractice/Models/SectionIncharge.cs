﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class SectionIncharge : BaseModel
    {
        #region BELONGS TO FIELDS

        public int TeacherId { get; set; }

        public int SectionId { get; set; }

        public virtual Teacher Teacher { get; set; }

        public virtual Section Section { get; set; }

        #endregion

    }
}
