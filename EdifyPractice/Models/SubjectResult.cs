﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class SubjectResult : BaseModel
    {
        #region BELONGS TO FIELDS

        public int SectionId { get; set; }

        public int ExamTestId { get; set; }

        public int SubjectId { get; set; }

        public int TeacherId { get; set; }


        public virtual Section Section { get; set; }
        public virtual ExamTest ExamTest { get; set; }
        public virtual Subject Subject { get; set; }
        public virtual Teacher Teacher { get; set; }

        #endregion


        #region HAS MANY FIELDS

        public virtual Collection<SubjectResultDetail> SubjectResultDetails { get; set; }

        #endregion


    }
}
