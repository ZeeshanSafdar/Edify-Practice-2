﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class NotificationDetail : BaseModel
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }

        public int RecieverId { get; set; }
        [Required]
        public string NotificationType { get; set; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime Date { get; set; }

    }
}
