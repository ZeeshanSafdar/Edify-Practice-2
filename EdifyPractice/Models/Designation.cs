﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class Designation : BaseModel
    {
     
        public int TotalEmployees{ get; set; }


        #region HAS MANY FIELDS

        public virtual Collection<Employee> Employees { get; set; }

        #endregion

    }
}
