﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class School : DefaultModel
    {

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [StringLength(16)]
        public string PhoneNo { get; set; }

        [Required]
        public string Logo { get; set; }

        [Required]
        [StringLength(150)]
        public string Address { get; set; }


        #region HAS MANY FIELDS

        public virtual Collection<NewsUpdate> NewsUpdates { get; set; }
        public virtual Collection<AcademicYear> AcademicYears { get; set; }
        public virtual Collection<Branch> Branches { get; set; }
        public virtual Collection<Department> Departments { get; set; }

        #endregion


    }
}
